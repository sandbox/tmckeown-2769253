<?php

namespace Drupal\prevoty\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PrevotySettingsForm.
 *
 * @package Drupal\prevoty\Form
 */
class PrevotySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'prevoty.PrevotySettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'prevoty_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('prevoty.PrevotySettings');
    $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('General settings'),
      '#collapsible' => FALSE,
    );
    $form['settings']['prevoty_api_base'] = array(
      '#default_value' => \Drupal::state()->get('prevoty_api_base') ?: 'https://api.prevoty.com',
      '#type' => 'textfield',
      '#title' => t('API Key'),
      '#description' => t('The api base url from prevoty.'),
      '#size' => 40,
    );
    $form['settings']['prevoty_api_key'] = array(
      '#default_value' => \Drupal::state()->get('prevoty_api_key') ?: '',
      '#type' => 'textfield',
      '#title' => t('API Key'),
      '#description' => t('The api key obtained from prevoty.'),
      '#size' => 40,
    );
    $form['settings']['prevoty_timeout'] = array(
      '#default_value' => \Drupal::state()->get('prevoty_timeout') ?: 5,
      '#type' => 'textfield',
      '#title' => t('Timeout in seconds'),
      '#description' => t('The amount of time a request should be sent in seconds before timing out.'),
      '#size' => 40,
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('prevoty.PrevotySettings')
      ->save();
  }

}
